<?php require_once('../private/initialize.php'); ?>
<?php

$preview = false;
if (isset($_GET['preview'])) {
    $preview = $_GET['preview'] == true && isLoggedIn() ? true : false;
}
$visible = !$preview;

if (isset($_GET['id'])) {
    $page_id = $_GET['id'];
    $page = findPageById($page_id, ['visible' => $visible]);
    $subject_id = $page['subject_id'];
    $subject = findSubjectById($subject_id, ['visible' => $visible]);
    if (!$subject) {
        header("Location: " . WWW_ROOT . "/index.php");
        die();
    }
    if (!$page) {
        header("Location: " . WWW_ROOT . "/index.php");
        die();
    }
} elseif (isset($_GET['subject_id'])) {
    $subject_id = $_GET['subject_id'];
    $subject = findSubjectById($subject_id, ['visible' => $visible]);
    if (!$subject) {
        header("Location: " . WWW_ROOT . "/index.php");
        die();
    }
    $pages = findPagesBySubjectId($subject_id, ['visible' => $visible]);
    $page = $pages[0];
    if (!$page) {
        header("Location: " . WWW_ROOT . "/index.php");
        die();
    }
    $page_id = $page['id'];
}
?>
<?php include(SHARED_PATH . '/public_header.php'); ?>

    <div id="main">

        <?php include(SHARED_PATH . '/public_navigation.php'); ?>

        <div id="page">
            <?php
            if (isset($page)) {
                // TODO
                $allowed_tags = '<div><h1><h2><p><br><strong><em><ul><li><img>';
                echo strip_tags($page['content'], $allowed_tags);
            } else {
                include(SHARED_PATH . '/static_homepage.php');
            }
            ?>
        </div>

    </div>

<?php include(SHARED_PATH . '/public_footer.php'); ?>