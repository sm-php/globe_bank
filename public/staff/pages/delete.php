<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/pages/index.php');
    die();
} else {
    $page_id = $_GET['id'];
}

$page = findPageById($page_id);

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $result = removePage($page_id);
    $_SESSION['msg'] = "The subject was deleted successfully";
    header("Location: " . WWW_ROOT . '/staff/subjects/show.php?subject_id=' . htmlspecialchars(urlencode($page['subject_id'])));
    die();
}

$page_title = "Delete Page"
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/subjects/show.php?subject_id=' . htmlspecialchars(urlencode($page['subject_id'])) ?>">&laquo; Back to Subject Page</a>
        <div class="subject delete">
            <h1>Delete Page</h1>
            <p>Are you sure you want to delete this subject?</p>
            <p class="item"><?php echo htmlspecialchars($page['menu_name']); ?></p>
            <form action="<?php echo WWW_ROOT . "/staff/pages/delete.php?id=" . htmlspecialchars(urlencode($page['id'])); ?>"
                  method="post">
                <div id="operations">
                    <input type="submit" name="commit" value="Delete Page"/>
                </div>
            </form>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>