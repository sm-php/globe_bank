<?php
require_once("../../../private/initialize.php");
requireLogin();

$subject_id = $_GET['subject_id'] ?? '';
$subjects = findAllSubjects();
$page_count = findPageCountBySubjectId($subject_id);
$page_count = $page_count + 1;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $page['subject_id'] = $_POST['subject_id'] ?? "";
    $page['menu_name'] = $_POST['menu_name'] ?? "";
    $page['position'] = $_POST['position'] ?? "";
    $page['visible'] = $_POST['visible'] ?? "";
    $page['content'] = $_POST['content'] ?? "";
    $result = createPage($page);
    if ($result === true) {
        $_SESSION['msg'] = "The page was created successfully";
        $new_page_id = mysqli_insert_id($db);
        header("Location: " . WWW_ROOT . '/staff/pages/show.php?id=' . $new_page_id);
        die();
    } else {
        $errors = $result;
        var_dump($errors);
    }
} else {
    $page = [];
    $page['subject_id'] = $subject_id;
    $page['menu_name'] = "";
    $page['position'] = $page_count;
    $page['visible'] = "";
    $page['content'] = "";
}

$page_title = 'Create Page';
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/subjects/show.php?subject_id=' . htmlspecialchars(urlencode($page['subject_id'])) ?>">&laquo; Back to Subject Page</a>
        <div class="page new">
            <h1>Create Page</h1>
            <?php echo display_errors($errors); ?>
            <form action="<?php echo WWW_ROOT . '/staff/pages/new.php'; ?>" method="post">
                <dl>
                    <dt>Menu Name</dt>
                    <dd>
                        <input type="text" name="menu_name"
                               value="<?php echo htmlspecialchars($page['menu_name']); ?>"/>
                    </dd>
                </dl>
                <dl>
                    <dt>Subject</dt>
                    <dd>
                        <select name="subject_id">
                            <?php foreach ($subjects as $subject) { ?>
                                <option value="<?php echo $subject['id'] ?>" <?php if ($page['subject_id'] === $subject['id']) {
                                    echo "selected";
                                } ?>><?php echo htmlspecialchars($subject['menu_name']); ?></option>
                            <?php } ?>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt>Position</dt>
                    <dd>
                        <select name="position">
                            <?php for ($i = 1; $i <= $page_count; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($page['position'] == $i) {
                                    echo "selected";
                                } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt>Visible</dt>
                    <dd>
                        <input type="hidden" name="visible" value="0"/>
                        <input type="checkbox" name="visible" value="1" <?php if ($page['visible'] === '1') {
                            echo "checked";
                        } ?> />
                    </dd>
                </dl>
                <dl>
                    <dt>Content</dt>
                    <dd>
                        <textarea name="content" cols="30"
                                  rows="10"><?php echo htmlspecialchars($page['content']); ?></textarea>
                    </dd>
                </dl>
                <div id="operations">
                    <input type="submit" value="Create Page"/>
                </div>
            </form>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>