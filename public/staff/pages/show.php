<?php
require_once("../../../private/initialize.php");
requireLogin();
$page_id = $_GET['id'] ?? '1';
$page = findPageById($page_id);
$page_title = "Show Page";
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/subjects/show.php?subject_id=' . htmlspecialchars(urlencode($page['subject_id'])) ?>">&laquo; Back to Subject Page</a>
        <div class="page show">
            <h1>Page: <?php echo htmlspecialchars($page['menu_name']); ?></h1>
            <div class="actions">
                <a href="<?php echo WWW_ROOT . '/index.php?id=' . htmlspecialchars(urlencode($page_id)) . '&preview=true'; ?>" class="actions" target="_blank">
                    Preview
                </a>
            </div>
            <div class="attributes">
                <dl>
                    <dt>Menu Name</dt>
                    <dd><?php echo htmlspecialchars($page['menu_name']); ?></dd>
                </dl>
                <dl>
                    <dt>Subject</dt>
                    <dd><?php echo htmlspecialchars($page['subject']); ?></dd>
                </dl>
                <dl>
                    <dt>Position</dt>
                    <dd><?php echo htmlspecialchars($page['position']); ?></dd>
                </dl>
                <dl>
                    <dt>Visible</dt>
                    <dd><?php echo $page['visible'] == '1' ? 'true' : 'false'; ?></dd>
                </dl>
                <dl>
                    <dt>Content</dt>
                    <dd><?php echo htmlspecialchars($page['content']); ?></dd>
                </dl>
            </div>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>