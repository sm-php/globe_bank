<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id']) || empty($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/pages/index.php');
    exit();
} else {
    $page_id = $_GET['id'];
}

$subjects = findAllSubjects();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $page['id'] = $page_id;
    $page['subject_id'] = $_POST['subject_id'] ?? "";
    $page['menu_name'] = $_POST['menu_name'] ?? "";
    $page['position'] = $_POST['position'] ?? "";
    $page['visible'] = $_POST['visible'] ?? "";
    $page['content'] = $_POST['content'] ?? "";
    $result = updatePage($page);
    if ($result === true) {
        $_SESSION['msg'] = "The subject was updated successfully";
        header("Location: " . WWW_ROOT . '/staff/pages/show.php?id=' . $page['id']);
        die();
    } else {
        $errors = $result;
    }
} else {
    $page = findPageById($page_id);
}
$page_count = findPageCountBySubjectId($page['subject_id']);
$page_title = 'Edit Page';
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/subjects/show.php?subject_id=' . htmlspecialchars(urlencode($page['subject_id'])) ?>">&laquo; Back to Subject Page</a>
        <div class="page new">
            <h1>Edit Page</h1>
            <?php echo display_errors($errors); ?>
            <form action="<?php echo WWW_ROOT . '/staff/pages/edit.php?id=' . $page_id; ?>" method="post">
                <dl>
                    <dt>Menu Name</dt>
                    <dd>
                        <input type="text" name="menu_name"
                               value="<?php echo htmlspecialchars($page['menu_name']); ?>"/>
                    </dd>
                </dl>
                <dl>
                    <dt>Subject</dt>
                    <dd>
                        <select name="subject_id">
                            <?php foreach ($subjects as $subject) { ?>
                                <option value="<?php echo $subject['id'] ?>" <?php if ($page['subject'] == $subject['menu_name']) {
                                    echo "selected";
                                } ?>><?php echo htmlspecialchars($subject['menu_name']); ?></option>
                            <?php } ?>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt>Position</dt>
                    <dd>
                        <select name="position">
                            <?php for ($i = 1; $i <= $page_count; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($page['position'] == $i) {
                                    echo "selected";
                                } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt>Visible</dt>
                    <dd>
                        <input type="hidden" name="visible" value="0"/>
                        <input type="checkbox" name="visible" value="1" <?php if ($page['visible'] === '1') {
                            echo "checked";
                        } ?> />
                    </dd>
                </dl>
                <dl>
                    <dt>Content</dt>
                    <dd>
                        <textarea name="content" cols="30"
                                  rows="10"><?php echo htmlspecialchars($page['content']); ?></textarea>
                    </dd>
                </dl>
                <div id="operations">
                    <input type="submit" value="Edit Page"/>
                </div>
            </form>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>