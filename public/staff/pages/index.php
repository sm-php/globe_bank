<?php
require_once("../../../private/initialize.php");
requireLogin();

// redirecting to staff/index.php because showing pages under subjects
header("Location: " . WWW_ROOT . '/staff/index.php');
die();

$pages = findAllPages();
$page_title = "Pages";
?>
<?php include_once(SHARED_PATH . "/staff_header.php") ?>
    <div id="content">
        <div class="pages listing">
            <h1>Pages</h1>
            <div class="actions">
                <a class="action" href="<?php echo WWW_ROOT . '/staff/pages/new.php' ?>">Create New Page</a>
            </div>
            <table class="list">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Subject</th>
                    <th>Position</th>
                    <th>Visible</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($pages as $page) { ?>
                    <tr>
                        <td><?php echo htmlspecialchars($page['id']); ?></td>
                        <td><?php echo htmlspecialchars($page['menu_name']); ?></td>
                        <td><?php echo htmlspecialchars($page['subject']); ?></td>
                        <td><?php echo htmlspecialchars($page['position']); ?></td>
                        <td><?php echo $page['visible'] == 1 ? 'true' : 'false'; ?></td>
                        <td>
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/show.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">View</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/edit.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">Edit</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/delete.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">Delete</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php") ?>