<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id']) || empty($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/admins/index.php');
    exit();
} else {
    $admin_id = $_GET['id'];
}

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $admin = [];
    $admin['id'] = $admin_id;
    $admin['first_name'] = $_POST['first_name'] ?? '';
    $admin['last_name'] = $_POST['last_name'] ?? '';
    $admin['email'] = $_POST['email'] ?? '';
    $admin['username'] = $_POST['username'] ?? '';
    $admin['password'] = $_POST['password'] ?? '';
    $admin['confirm_password'] = $_POST['confirm_password'] ?? '';

    $result = updateAdmin($admin);
    if ($result === true) {
        $_SESSION['msg'] = "The admin was updated successfully.";
        header("Location: " . WWW_ROOT . '/staff/admins/show.php?id=' . $admin_id);
        die();
    } else {
        $errors = $result;
    }
} else {
    $admin = findAdminById($admin_id);
}

$page_title = "Update Admin";
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
<div id="content">
    <a class="back-link" href="<?php echo WWW_ROOT . '/staff/admins/index.php'; ?>">&laquo; Back to List</a>
    <div class="admin new">
        <h1>Update Admin</h1>
        <?php echo display_errors($errors); ?>
        <form action="<?php echo WWW_ROOT . '/staff/admins/edit.php?id=' . $admin_id; ?>" method="post">
            <dl>
                <dt>First Name</dt>
                <dd>
                    <input type="text" name="first_name" value="<?php echo htmlspecialchars($admin['first_name']); ?>"/>
                </dd>
            </dl>
            <dl>
                <dt>Last Name</dt>
                <dd>
                    <input type="text" name="last_name" value="<?php echo htmlspecialchars($admin['last_name']); ?>"/>
                </dd>
            </dl>
            <dl>
                <dt>Email</dt>
                <dd>
                    <input type="text" name="email" value="<?php echo htmlspecialchars($admin['email']); ?>"/>
                </dd>
            </dl>
            <dl>
                <dt>Username</dt>
                <dd>
                    <input type="text" name="username" value="<?php echo htmlspecialchars($admin['username']); ?>"/>
                </dd>
            </dl>
            <dl>
                <dt>Password</dt>
                <dd>
                    <input type="password" name="password" value=""/>
                </dd>
            </dl>
            <dl>
                <dt>Confirm Password</dt>
                <dd>
                    <input type="password" name="confirm_password" value=""/>
                </dd>
            </dl>
            <p>
                Password should be at least 12 characters and include at least one uppercase letter, lowercase letter, number and symbol.
            </p>
            <div id="operations">
                <input type="submit" value="Update Admin"/>
            </div>
        </form>
    </div>
</div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>