<?php
require_once("../../../private/initialize.php");
requireLogin();
$admins = findAllAdmins();
$page_title = "Admins";
?>
<?php include_once(SHARED_PATH . "/staff_header.php") ?>
    <div id="content">
        <div class="admins listing">
            <h1>Admins</h1>
            <div class="actions">
                <a class="action" href="<?php echo WWW_ROOT . '/staff/admins/new.php' ?>">Create New Admin</a>
            </div>
            <table class="list">
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($admins as $admin) { ?>
                    <tr>
                        <td><?php echo htmlspecialchars($admin['id']); ?></td>
                        <td><?php echo htmlspecialchars($admin['first_name']); ?></td>
                        <td><?php echo htmlspecialchars($admin['last_name']); ?></td>
                        <td><?php echo htmlspecialchars($admin['email']); ?></td>
                        <td><?php echo htmlspecialchars($admin['username']); ?></td>
                        <td>
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/admins/show.php?id=') . htmlspecialchars(urlencode($admin['id'])); ?>">View</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/admins/edit.php?id=') . htmlspecialchars(urlencode($admin['id'])); ?>">Edit</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/admins/delete.php?id=') . htmlspecialchars(urlencode($admin['id'])); ?>">Delete</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php") ?>