<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/admins/index.php');
    die();
} else {
    $admin_id = $_GET['id'];
}

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $result = removeAdmin($admin_id);
    $_SESSION['msg'] = "The admin was deleted successfully";
    header("Location: " . WWW_ROOT . '/staff/admins/index.php');
    die();
} else {
    $admin = findAdminById($admin_id);
}

$page_title = "Delete Admin"
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a class="back-link" href="<?php echo WWW_ROOT . '/staff/admins/index.php'; ?>">&laquo; Back to List</a>
        <div class="admin delete">
            <h1>Delete Admin</h1>
            <p>Are you sure you want to delete this admin?</p>
            <p class="item"><?php echo htmlspecialchars($admin['username']); ?></p>
            <form action="<?php echo WWW_ROOT . "/staff/admins/delete.php?id=" . htmlspecialchars(urlencode($admin['id'])); ?>"
                  method="post">
                <div id="operations">
                    <input type="submit" name="commit" value="Delete Admin"/>
                </div>
            </form>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>