<?php
require_once("../../../private/initialize.php");
requireLogin();
$admin_id = $_GET['id'] ?? '1';
$admin = findAdminById($admin_id);
$page_title = "Show Admin";
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/admins/index.php' ?>">&laquo; Back to list</a>
        <div class="admin show">
            <h1>Admin: <?php echo htmlspecialchars($admin['username']); ?></h1>
            <div class="attributes">
                <dl>
                    <dt>First Name</dt>
                    <dd><?php echo htmlspecialchars($admin['first_name']); ?></dd>
                </dl>
                <dl>
                    <dt>Last Name</dt>
                    <dd><?php echo htmlspecialchars($admin['last_name']); ?></dd>
                </dl>
                <dl>
                    <dt>Email</dt>
                    <dd><?php echo htmlspecialchars($admin['email']); ?></dd>
                </dl>
                <dl>
                    <dt>Username</dt>
                    <dd><?php echo htmlspecialchars($admin['username']); ?></dd>
                </dl>
            </div>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>