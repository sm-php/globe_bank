<?php
require_once('../../../private/initialize.php');
requireLogin();
$subjects = findAllSubjects();
$page_title = 'Subjects';
?>
<?php include(SHARED_PATH . '/staff_header.php'); ?>
    <div id="content">
        <div class="subjects listing">
            <h1>Subjects</h1>
            <div class="actions">
                <a class="action" href="<?php echo WWW_ROOT . '/staff/subjects/new.php'; ?>">Create New Subject</a>
            </div>
            <table class="list">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Pages</th>
                    <th>Position</th>
                    <th>Visible</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($subjects as $subject) { ?>
                    <tr>
                        <td><?php echo htmlspecialchars($subject['id']); ?></td>
                        <td><?php echo htmlspecialchars($subject['menu_name']); ?></td>
                        <td><?php echo htmlspecialchars($subject['page_count']); ?></td>
                        <td><?php echo htmlspecialchars($subject['position']); ?></td>
                        <td><?php echo $subject['visible'] == 1 ? 'true' : 'false'; ?></td>
                        <td>
                            <a class="action"
                               href="<?php echo WWW_ROOT . '/staff/subjects/show.php?id=' . htmlspecialchars(urlencode($subject['id'])); ?>">View</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo WWW_ROOT . '/staff/subjects/edit.php?id=' . htmlspecialchars(urlencode($subject['id'])); ?>">Edit</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo WWW_ROOT . '/staff/subjects/delete.php?id=' . htmlspecialchars(urlencode($subject['id'])); ?>">Delete</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
<?php include(SHARED_PATH . '/staff_footer.php'); ?>