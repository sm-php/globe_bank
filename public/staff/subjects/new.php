<?php
require_once("../../../private/initialize.php");
requireLogin();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $subject['menu_name'] = $_POST['menu_name'] ?? "";
    $subject['position'] = $_POST['position'] ?? "";
    $subject['visible'] = $_POST['visible'] ?? "";
    $result = createSubject($subject);
    if ($result === true) {
        $_SESSION['msg'] = "The subject was created successfully";
        $new_subject_id = mysqli_insert_id($db);
        header("Location: " . WWW_ROOT . '/staff/subjects/show.php?id=' . $new_subject_id);
        die();
    } else {
        $errors = $result;
    }
} else {
    $subject = [];
    $subject['menu_name'] = "";
    $subject['position'] = "";
    $subject['visible'] = "";
}

$subject_count = findSubjectCount();
$subject_count = $subject_count + 1;
$subject['position'] = $subject_count;
$page_title = 'Create Subject';
?>
<?php include(SHARED_PATH . '/staff_header.php'); ?>
<div id="content">
    <a class="back-link" href="<?php echo WWW_ROOT . '/staff/subjects/index.php'; ?>">&laquo; Back to List</a>
    <div class="subject new">
        <h1>Create Subject</h1>
        <?php echo display_errors($errors); ?>
        <form action="<?php echo WWW_ROOT . '/staff/subjects/new.php'; ?>" method="post">
            <dl>
                <dt>Menu Name</dt>
                <dd><input type="text" name="menu_name" value="<?php echo htmlspecialchars($subject['menu_name']); ?>"/>
                </dd>
            </dl>
            <dl>
                <dt>Position</dt>
                <dd>
                    <select name="position">
                        <?php for ($i = 1; $i <= $subject_count; $i++) { ?>
                            <option value="<?php echo $i; ?>" <?php if ($subject['position'] == $i) {
                                echo "selected";
                            } ?>><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                </dd>
            </dl>
            <dl>
                <dt>Visible</dt>
                <dd>
                    <input type="hidden" name="visible" value="0"/>
                    <input type="checkbox" name="visible" value="1" <?php if ($subject['visible'] === '1') {
                        echo "checked";
                    } ?> />
                </dd>
            </dl>
            <div id="operations">
                <input type="submit" value="Create Subject"/>
            </div>
        </form>
    </div>
</div>
<?php include(SHARED_PATH . '/staff_footer.php'); ?>
