<?php
require_once("../../../private/initialize.php");
requireLogin();
$subject_id = $_GET['id'] ?? '1';
$subject = findSubjectById($subject_id);
$pages = findPagesBySubjectId($subject_id);
$page_title = "Show Subject";
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a href="<?php echo WWW_ROOT . '/staff/subjects/index.php' ?>">&laquo; Back to list</a>
        <div class="subject show">
            <h1>Subject: <?php echo htmlspecialchars($subject['menu_name']); ?></h1>
            <div class="attributes">
                <dl>
                    <dt>Menu Name</dt>
                    <dd><?php echo htmlspecialchars($subject['menu_name']); ?></dd>
                </dl>
                <dl>
                    <dt>Position</dt>
                    <dd><?php echo htmlspecialchars($subject['position']); ?></dd>
                </dl>
                <dl>
                    <dt>Visible</dt>
                    <dd><?php echo $subject['visible'] == '1' ? 'true' : 'false'; ?></dd>
                </dl>
            </div>
        </div>

        <hr>

        <div class="pages listing">
            <h2>Pages</h2>
            <div class="actions">
                <a class="action" href="<?php echo WWW_ROOT . '/staff/pages/new.php?subject_id=' . $subject_id ?>">Create New Page</a>
            </div>
            <table class="list">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Visible</th>
                    <th>Action</th>
                </tr>
                <?php foreach ($pages as $page) { ?>
                    <tr>
                        <td><?php echo htmlspecialchars($page['id']); ?></td>
                        <td><?php echo htmlspecialchars($page['menu_name']); ?></td>
                        <td><?php echo htmlspecialchars($page['position']); ?></td>
                        <td><?php echo $page['visible'] == 1 ? 'true' : 'false'; ?></td>
                        <td>
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/show.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">View</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/edit.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">Edit</a>&nbsp;&nbsp;
                            <a class="action"
                               href="<?php echo rawurldecode(WWW_ROOT . '/staff/pages/delete.php?id=') . htmlspecialchars(urlencode($page['id'])); ?>">Delete</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>