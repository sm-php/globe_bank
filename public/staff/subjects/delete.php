<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/subjects/index.php');
    die();
} else {
    $subject_id = $_GET['id'];
}

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $result = removeSubject($subject_id);
    $_SESSION['msg'] = "The subject was deleted successfully";
    header("Location: " . WWW_ROOT . '/staff/subjects/index.php');
    die();
} else {
    $subject = findSubjectById($subject_id);
}

$page_title = "Delete Subject"
?>
<?php include_once(SHARED_PATH . "/staff_header.php"); ?>
    <div id="content">
        <a class="back-link" href="<?php echo WWW_ROOT . '/staff/subjects/index.php'; ?>">&laquo; Back to List</a>
        <div class="subject delete">
            <h1>Delete Subject</h1>
            <p>Are you sure you want to delete this subject?</p>
            <p class="item"><?php echo htmlspecialchars($subject['menu_name']); ?></p>
            <form action="<?php echo WWW_ROOT . "/staff/subjects/delete.php?id=" . htmlspecialchars(urlencode($subject['id'])); ?>"
                  method="post">
                <div id="operations">
                    <input type="submit" name="commit" value="Delete Subject"/>
                </div>
            </form>
        </div>
    </div>
<?php include_once(SHARED_PATH . "/staff_footer.php"); ?>