<?php
require_once("../../../private/initialize.php");
requireLogin();
if (!isset($_GET['id']) || empty($_GET['id'])) {
    header("Location: " . WWW_ROOT . '/staff/subjects/index.php');
    exit();
} else {
    $subject_id = $_GET['id'];
}

$subject_count = findSubjectCount();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $subject = [];
    $subject['id'] = $subject_id;
    $subject['menu_name'] = $_POST['menu_name'] ?? '';
    $subject['position'] = $_POST['position'] ?? '';
    $subject['visible'] = $_POST['visible'] ?? '';
    $result = updateSubject($subject);
    if ($result === true) {
        $_SESSION['msg'] = "The subject was updated successfully";
        header("Location: " . WWW_ROOT . '/staff/subjects/show.php?id=' . $subject['id']);
        die();
    } else {
        $errors = $result;
    }
} else {
    $subject = findSubjectById($subject_id);
}

$page_title = 'Edit Subject';
?>
<?php include(SHARED_PATH . '/staff_header.php'); ?>
    <div id="content">
        <a class="back-link" href="<?php echo WWW_ROOT . '/staff/subjects/index.php'; ?>">&laquo; Back to List</a>
        <div class="subject edit">
            <h1>Edit Subject</h1>
            <?php echo display_errors($errors); ?>
            <form action="<?php echo WWW_ROOT . '/staff/subjects/edit.php?id=' . $subject_id; ?>" method="post">
                <dl>
                    <dt>Menu Name</dt>
                    <dd>
                        <input type="text" name="menu_name"
                               value="<?php echo htmlspecialchars($subject['menu_name']); ?>"/>
                    </dd>
                </dl>
                <dl>
                    <dt>Position</dt>
                    <dd>
                        <select name="position">
                            <?php for ($i = 1; $i <= $subject_count; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($subject['position'] == $i) {
                                    echo "selected";
                                } ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </dd>
                </dl>
                <dl>
                    <dt>Visible</dt>
                    <dd>
                        <input type="hidden" name="visible" value="0"/>
                        <input type="checkbox" name="visible" value="1" <?php if ($subject['visible'] === '1') {
                            echo "checked";
                        } ?> />
                    </dd>
                </dl>
                <div id="operations">
                    <input type="submit" value="Edit Subject"/>
                </div>
            </form>
        </div>
    </div>
<?php include(SHARED_PATH . '/staff_footer.php'); ?>