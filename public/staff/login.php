<?php
require_once('../../private/initialize.php');

$errors = [];
$username = '';
$password = '';

if ($_SERVER['REQUEST_METHOD'] === "POST") {

    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    if (is_blank($username)) {
        $errors[] = "Username can not be blank.";
    }

    if (is_blank($password)) {
        $errors[] = "Password can not be blank.";
    }

    if (empty($errors)) {
        $admin = findAdminByUsername($username);
        $login_failed_message = "Log in unsuccessful.";
        if ($admin) {
            if (password_verify($password, $admin['hashed_password'])) {
                // password matches
                loginAdmin($admin);
                header("Location: " . WWW_ROOT . '/staff/index.php');
                die();
            } else {
                // username found, but password does not match
                $errors[] = $login_failed_message;
            }
        } else {
            // no username found
            $errors[] = $login_failed_message;
        }
    }
}

?>

<?php $page_title = 'Log in'; ?>
<?php include(SHARED_PATH . '/staff_header.php'); ?>

<div id="content">
    <h1>Log in</h1>

    <?php echo display_errors($errors); ?>

    <form action="login.php" method="post">
        Username:<br/>
        <input type="text" name="username" value="<?php echo htmlspecialchars($username); ?>"/><br/>
        Password:<br/>
        <input type="password" name="password" value=""/><br/>
        <input type="submit" name="submit" value="Submit"/>
    </form>

</div>

<?php include(SHARED_PATH . '/staff_footer.php'); ?>
