<?php

function error404()
{
    header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found.");
    exit();
}

function error500()
{
    header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error.");
    exit();
}

function display_errors($errors)
{
    $output = "";
    if (!empty($errors)) {
        $output .= "<div class='errors'>";
        $output .= "Please fix the following erros.";
        $output .= "<ul>";
        foreach ($errors as $error) {
            $output .= "<li>" . htmlspecialchars($error) . "</li>";
        }
        $output .= "</li>";
        $output .= "</div>";
    }

    return $output;
}

function getAndClearSessionMessage() {
    if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') {
        $msg = $_SESSION['msg'];
        unset($_SESSION['msg']);
        return $msg;
    }
}

function displaySessionMessage() {
    $msg = getAndClearSessionMessage();
    if (!is_blank($msg)) {
        return "<div id='message'>" . htmlspecialchars($msg) . "</div>";
    }
}