<?php
require_once("db_credentials.php");

function db_connect()
{
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    confirm_db_connection();
    return $connection;
}

function db_disconnect($connection)
{
    if (isset($connection)) {
        mysqli_close($connection);
    }
}

function confirm_db_connection()
{
    if (mysqli_connect_error()) {
        $msg = "Database Connection Failed: ";
        $msg .= mysqli_connect_error();
        $msg .= " ( " . mysqli_connect_errno() . " ) ";
        die($msg);
    }
}

function confirm_result_set($result)
{
    if (!$result) {
        die("Database query failed.");
    }
}