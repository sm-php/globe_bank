<!doctype html>

<html lang="en">
<head>
    <title>Globe Bank <?php if (isset($page_title)) {
            echo '- ' . htmlspecialchars($page_title);
        } ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="all" href="<?php echo WWW_ROOT . '/css/public.css'; ?>"/>
</head>

<body>

<header>
    <h1>
        <a href="<?php echo WWW_ROOT . '/index.php'; ?>">
            <img src="<?php echo WWW_ROOT . '/img/gbi_logo.png'; ?>" width="298" height="71" alt=""/>
        </a>
    </h1>
</header>