<?php
$page_id = $page_id ?? '';
$subject_id = $subject_id ?? '';
$visible = $visible ?? true;
?>
<nav>
    <?php $nav_subjects = findAllSubjects(['visible' => $visible]); ?>
    <ul class="subjects">
        <?php foreach ($nav_subjects as $nav_subject) { ?>
            <li class="<?php if ($nav_subject['id'] == $subject_id) { echo "selected"; } ?>">
                <a href="<?php echo WWW_ROOT . "/index.php?subject_id=" . htmlspecialchars(urlencode($nav_subject['id'])); ?>">
                    <?php echo htmlspecialchars($nav_subject['menu_name']); ?>
                </a>
                <?php if ($nav_subject['id'] == $subject_id) { ?>
                    <?php $nav_pages = findPagesBySubjectId($nav_subject['id'], ['visible' => $visible]); ?>
                    <ul class="pages">
                        <?php foreach ($nav_pages as $nav_page) { ?>
                        <li class="<?php if ($nav_page['id'] == $page_id) { echo "selected"; } ?>">
                            <a href="<?php echo WWW_ROOT . "/index.php?id=" . htmlspecialchars(urlencode($nav_page['id'])); ?>">
                                <?php echo $nav_page['menu_name']; ?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
</nav>