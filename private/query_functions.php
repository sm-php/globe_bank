<?php

function findAllSubjects($options = [])
{
    global $db;

    $visible = $options['visible'] ?? false;

    $sql = "SELECT `subjects`.`id`, `subjects`.`menu_name`, COUNT(`pages`.`id`) AS page_count, `subjects`.`position`, `subjects`.`visible` ";
    $sql .= "FROM `subjects` ";
    $sql .= "LEFT JOIN `pages` ";
    $sql .= "ON `subjects`.`id` = `pages`.`subject_id` ";
    $sql .= "GROUP BY `subjects`.`menu_name` ";

    if ($visible) {
        $sql .= "WHERE `visible` = true ";
    }
    $sql .= "ORDER BY `position` ASC;";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $subjects = [];
    while ($subject = mysqli_fetch_assoc($result)) {
        $subjects[] = $subject;
    }
    mysqli_free_result($result);
    return $subjects;
}

function findSubjectById($subject_id, $options = [])
{
    global $db;

    $visible = $options['visible'] ?? '';

    $sql = "SELECT `id`, `menu_name`, `position`, `visible` ";
    $sql .= "FROM `subjects` ";
    $sql .= "WHERE `id` = '" . mysqli_real_escape_string($db, $subject_id) . "' ";
    if ($visible) {
        $sql .= "AND `visible` = true ";
    }
    $sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $subject = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $subject;
}

function validateSubject($subject)
{
    $errors = [];
    // menu_name
    if (is_blank($subject['menu_name'])) {
        $errors[] = "Name can not be blank.";
    } elseif (!has_length($subject['menu_name'], ['min' => 2, 'max' => 255])) {
        $errors[] = "Name must be between 2 and 255 characters";
    }

    // position
    $position = (int)$subject['position'];
    if ($position <= 0) {
        $errors[] = "Position must be greater than zero.";
    }
    if ($position > 999) {
        $errors[] = "Position must be less than 999";
    }

    // visible
    $visible_str = (string)$subject['visible'];
    if (!has_inclusion_of($visible_str, ["0", "1"])) {
        $errors[] = "Visible must be true or false";
    }

    return $errors;
}

function shiftSubjectPosition($start, $end, $current_id = 0)
{
    global $db;
    if ($start == $end) {
        return;
    }

    $sql = "UPDATE `subjects` ";
    if ($start == 0) {
        $sql .= "SET `position` = `position` + 1 ";
        $sql .= "WHERE `position` >= '" . mysqli_real_escape_string($db, $end) . "' ";
    } elseif ($end == 0) {
        $sql .= "SET `position` = `position` - 1 ";
        $sql .= "WHERE `position` > '" . mysqli_real_escape_string($db, $start) . "' ";
    } elseif ($start < $end) {
        $sql .= "SET `position` = `position` - 1 ";
        $sql .= "WHERE `position` > '" . mysqli_real_escape_string($db, $start) . "' ";
        $sql .= "AND `position` <= '" . mysqli_real_escape_string($db, $end) . "' ";
    } elseif ($start > $end) {
        $sql .= "SET `position` = `position` + 1 ";
        $sql .= "WHERE `position` >= '" . mysqli_real_escape_string($db, $end) . "' ";
        $sql .= "AND `position` < '" . mysqli_real_escape_string($db, $start) . "' ";
    }
    $sql .= "AND `id` != '" . mysqli_real_escape_string($db, $current_id) . "';";

    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function createSubject($subject)
{
    global $db;

    $errors = validateSubject($subject);
    if (!empty($errors)) {
        return $errors;
    }

    shiftSubjectPosition(0, $subject['position']);

    $sql = "INSERT INTO `subjects` ";
    $sql .= "(`menu_name`, `position`, `visible`) ";
    $sql .= "VALUES (";
    $sql .= "'" . mysqli_real_escape_string($db, $subject['menu_name']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $subject['position']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $subject['visible']) . "'";
    $sql .= ");";

    $result = mysqli_query($db, $sql);

    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function updateSubject($subject)
{
    global $db;

    $errors = validateSubject($subject);
    if (!empty($errors)) {
        return $errors;
    }
    $old_subject = findSubjectById($subject['id']);
    shiftSubjectPosition($old_subject['position'], $subject['position'], $subject['id']);

    $sql = "UPDATE `subjects` SET ";
    $sql .= "`menu_name`='" . mysqli_real_escape_string($db, $subject['menu_name']) . "', ";
    $sql .= "`position`='" . mysqli_real_escape_string($db, $subject['position']) . "', ";
    $sql .= "`visible`='" . mysqli_real_escape_string($db, $subject['visible']) . "' ";
    $sql .= "WHERE `id` = '" . mysqli_real_escape_string($db, $subject['id']) . "' ";
    $sql .= "LIMIT 1";

    $result = mysqli_query($db, $sql);

    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function removeSubject($subject_id)
{
    global $db;

    $old_subject = findSubjectById($subject_id);
    shiftSubjectPosition($old_subject['position'], 0, $subject_id);

    $sql = "DELETE FROM `subjects` ";
    $sql .= "WHERE id='" . mysqli_real_escape_string($db, $subject_id) . "' ";
    $sql .= "LIMIT 1;";
    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function findSubjectCount()
{
    global $db;
    $sql = "SELECT COUNT(*) AS count ";
    $sql .= "FROM `subjects`;";

    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $subject_count = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $subject_count['count'];
}

function findAllPages()
{
    global $db;
    $sql = "SELECT `pages`.`id`, `pages`.`subject_id`, `subjects`.`menu_name` AS subject, `pages`.`menu_name`, `pages`.`position`, `pages`.`visible`, `pages`.`content` ";
    $sql .= "FROM `pages` ";
    $sql .= "LEFT JOIN `subjects` ";
    $sql .= "ON `pages`.`subject_id` = `subjects`.`id` ";
    $sql .= "ORDER BY `subjects`.`id`, `pages`.`position` ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $pages = [];
    while ($page = mysqli_fetch_assoc($result)) {
        $pages[] = $page;
    }
    mysqli_free_result($result);
    return $pages;
}

function findPageById($page_id, $options = [])
{
    global $db;

    $visible = $options['visible'] ?? '';

    $sql = "SELECT `pages`.`id`, `pages`.`subject_id`, `subjects`.`menu_name` AS subject, `pages`.`menu_name`, `pages`.`position`, `pages`.`visible`, `pages`.`content` ";
    $sql .= "FROM `pages` ";
    $sql .= "LEFT JOIN `subjects` ";
    $sql .= "ON `pages`.`subject_id` = `subjects`.`id` ";
    $sql .= "WHERE `pages`.`id` = '" . mysqli_real_escape_string($db, $page_id) . "'";
    if ($visible) {
        $sql .= " AND `pages`.`visible` = true";
    }
    //$sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $page = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $page;
}

function findPagesBySubjectId($subject_id, $options = [])
{
    global $db;

    $visible = $options['visible'] ?? '';

    $sql = "SELECT `id`, `subject_id`, `menu_name`, `position`, `visible`, `content` ";
    $sql .= "FROM `pages` ";
    $sql .= "WHERE `subject_id` = '" . mysqli_real_escape_string($db, $subject_id) . "' ";
    if ($visible) {
        $sql .= "AND `visible` = true ";
    }
    $sql .= "ORDER BY `position` ASC";

    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $pages = [];
    while ($page = mysqli_fetch_assoc($result)) {
        $pages[] = $page;
    }
    mysqli_free_result($result);
    return $pages;
}

function validatePage($page)
{
    $errors = [];
    // subject_id
    if (is_blank($page['subject_id'])) {
        $errors[] = "Subject can not be blank.";
    }

    // menu_name
    if (is_blank($page['menu_name'])) {
        $errors[] = "Name can not be blank.";
    } elseif (!has_length($page['menu_name'], ['min' => 2, 'max' => 255])) {
        $errors[] = "Name must be between 2 and 255 characters";
    }

    // position
    $position = (int)$page['position'];

    if ($position <= 0) {
        $errors[] = "Position must be greater than zero.";
    }
    if ($position > 999) {
        $errors[] = "Position must be less than 999";
    }

    // visible
    $visible_str = (string)$page['visible'];
    if (!has_inclusion_of($visible_str, ["0", "1"])) {
        $errors[] = "Visible must be true or false";
    }

    // content
    if (is_blank($page['content'])) {
        $errors[] = "Content can not be blank.";
    }

    return $errors;
}

function shiftPagePosition($start, $end, $subject_id, $current_id = 0)
{
    global $db;
    if ($start == $end) {
        return;
    }

    $sql = "UPDATE `pages` ";
    if ($start == 0) {
        $sql .= "SET `position` = `position` + 1 ";
        $sql .= "WHERE `position` >= '" . mysqli_real_escape_string($db, $end) . "' ";
    } elseif ($end == 0) {
        $sql .= "SET `position` = `position` - 1 ";
        $sql .= "WHERE `position` > '" . mysqli_real_escape_string($db, $start) . "' ";
    } elseif ($start < $end) {
        $sql .= "SET `position` = `position` - 1 ";
        $sql .= "WHERE `position` > '" . mysqli_real_escape_string($db, $start) . "' ";
        $sql .= "AND `position` <= '" . mysqli_real_escape_string($db, $end) . "' ";
    } elseif ($start > $end) {
        $sql .= "SET `position` = `position` + 1 ";
        $sql .= "WHERE `position` >= '" . mysqli_real_escape_string($db, $end) . "' ";
        $sql .= "AND `position` < '" . mysqli_real_escape_string($db, $start) . "' ";
    }
    $sql .= "AND `id` != '" . mysqli_real_escape_string($db, $current_id) . "' ";
    $sql .= "AND `subject_id` = '" . mysqli_real_escape_string($db, $subject_id) . "';";

    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function createPage($page)
{
    global $db;

    $errors = validatePage($page);
    if (!empty($errors)) {
        return $errors;
    }

    shiftPagePosition(0, $page['position'], $page['subject_id']);

    $sql = "INSERT INTO `pages` (`subject_id`, `menu_name`, `position`, `visible`, `content`) ";
    $sql .= "VALUES (";
    $sql .= "'" . mysqli_real_escape_string($db, $page['subject_id']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $page['menu_name']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $page['position']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $page['visible']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $page['content']) . "'";
    $sql .= ");";

    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function updatePage($page)
{
    global $db;

    $errors = validatePage($page);
    if (!empty($errors)) {
        return $errors;
    }

    $old_page = findPageById($page['id']);
    shiftPagePosition($old_page['position'], $page['position'], $page['subject_id'], $page['id']);

    $sql = "UPDATE `pages` SET ";
    $sql .= "`subject_id` = '" . mysqli_real_escape_string($db, $page['subject_id']) . "', ";
    $sql .= "`menu_name` = '" . mysqli_real_escape_string($db, $page['menu_name']) . "', ";
    $sql .= "`position` = '" . mysqli_real_escape_string($db, $page['position']) . "', ";
    $sql .= "`visible` = '" . mysqli_real_escape_string($db, $page['visible']) . "', ";
    $sql .= "`content` = '" . mysqli_real_escape_string($db, $page['content']) . "' ";
    $sql .= "WHERE `id` = '" . mysqli_real_escape_string($db, $page['id']) . "' ";
    $sql .= "LIMIT 1";

    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function removePage($page_id)
{
    global $db;

    $old_page = findPageById($page_id);
    shiftPagePosition($old_page['position'], 0, $old_page['subject_id'], $page_id);

    $sql = "DELETE FROM `pages` ";
    $sql .= "WHERE id='" . mysqli_real_escape_string($db, $page_id) . "' ";
    $sql .= "LIMIT 1;";
    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function findPageCountBySubjectId($subject_id)
{
    global $db;
    $sql = "SELECT COUNT(*) AS count ";
    $sql .= "FROM `pages` ";
    $sql .= "WHERE `pages`.`subject_id` = '" . mysqli_real_escape_string($db, $subject_id) . "';";

    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $page_count = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $page_count['count'];
}

function findAllAdmins()
{
    global $db;
    $sql = "SELECT `id`, `first_name`, `last_name`, `email`, `username` ";
    $sql .= "FROM `admins` ";
    $sql .= "ORDER BY `last_name` ASC, `first_name` ASC;";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $admins = [];
    while ($admin = mysqli_fetch_assoc($result)) {
        $admins[] = $admin;
    }
    mysqli_free_result($result);
    return $admins;
}

function findAdminById($admin_id)
{
    global $db;
    $sql = "SELECT `id`, `first_name`, `last_name`, `email`, `username` ";
    $sql .= "FROM `admins` ";
    $sql .= "WHERE `id` = '" . mysqli_real_escape_string($db, $admin_id) . "' ";
    $sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $admin = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $admin;
}

function findAdminByUsername($username)
{
    global $db;
    $sql = "SELECT `id`, `first_name`, `last_name`, `email`, `username`, `hashed_password` ";
    $sql .= "FROM `admins` ";
    $sql .= "WHERE `username` = '" . mysqli_real_escape_string($db, $username) . "' ";
    $sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $admin = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $admin;
}

function validateAdmin($admin, $options = [])
{
    $errors = [];
    $password_required = $options['password_required'] ?? true;
    // first_name
    if (is_blank($admin['first_name'])) {
        $errors[] = "First name can not be blank.";
    } elseif (!has_length($admin['first_name'], ['min' => 2, 'max' => 255])) {
        $errors[] = "First Name must be between 2 and 255 characters";
    }

    // last_name
    if (is_blank($admin['last_name'])) {
        $errors[] = "Last name can not be blank.";
    } elseif (!has_length($admin['last_name'], ['min' => 2, 'max' => 255])) {
        $errors[] = "Last Name must be between 2 and 255 characters";
    }

    // email
    if (is_blank($admin['email'])) {
        $errors[] = "Email can not be blank.";
    }
    if (!has_valid_email_format($admin['email'])) {
        $errors[] = "Please enter valid email address.";
    }

    // username
    if (is_blank($admin['username'])) {
        $errors[] = "Username can not be blank.";
    } elseif (!has_length($admin['username'], ['min' => 8, 'max' => 255])) {
        $errors[] = "Username must be between 2 and 255 characters";
    } elseif (!hasUniqueUsername($admin['username'], $admin['id'] ?? 0)) {
        $errors[] = "Username already taken.";
    }

    // password
    if ($password_required) {
        if (is_blank($admin['password'])) {
            $errors[] = "Password can not be blank.";
        } elseif (!has_length($admin['password'], ['min' => 12, 'max' => 255])) {
            $errors[] = "Password must be between 2 and 255 characters.";
        } elseif (!preg_match('/[A-Z]/', $admin['password'])) {
            $errors[] = "Password must be contain at least uppercase letter.";
        } elseif (!preg_match('/[a-z]/', $admin['password'])) {
            $errors[] = "Password must be contain at least lowercase letter.";
        } elseif (!preg_match('/[0-9]/', $admin['password'])) {
            $errors[] = "Password must be contain at least one number.";
        } elseif (!preg_match('/[^A-Za-z0-9\s]/', $admin['password'])) {
            $errors[] = "Password must be contain at least special character.";
        }

        // confirm_password
        if (is_blank($admin['confirm_password'])) {
            $errors[] = "Confirm password can not be blank.";
        } elseif ($admin['confirm_password'] !== $admin['password']) {
            $errors[] = "Password and confirm password does not match.";
        }
    }

    return $errors;
}

function hasUniqueUsername($username, $current_id = "0")
{
    global $db;

    $sql = "SELECT * ";
    $sql .= "FROM admins ";
    $sql .= "WHERE username = '" . mysqli_real_escape_string($db, $username) . "' ";
    $sql .= "AND id != '" . mysqli_real_escape_string($db, $current_id) . "';";

    $result = mysqli_query($db, $sql);
    $admin_count = mysqli_num_rows($result);
    mysqli_free_result($result);
    return $admin_count === 0;
}

function createAdmin($admin)
{
    global $db;

    $errors = validateAdmin($admin);
    if (!empty($errors)) {
        return $errors;
    }

    $hashed_password = password_hash($admin['password'], PASSWORD_BCRYPT);

    $sql = "INSERT INTO `admins` ";
    $sql .= "(`first_name`, `last_name`, `email`, `username`, `hashed_password`) ";
    $sql .= "VALUES (";
    $sql .= "'" . mysqli_real_escape_string($db, $admin['first_name']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $admin['last_name']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $admin['email']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $admin['username']) . "', ";
    $sql .= "'" . mysqli_real_escape_string($db, $hashed_password) . "'";
    $sql .= ");";

    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function updateAdmin($admin)
{
    global $db;

    $password_sent = !is_blank($admin['password']);

    $errors = validateAdmin($admin, ['password_required' => $password_sent]);
    if (!empty($errors)) {
        echo "here";
        return $errors;
    }

    $password = password_hash($admin['password'], PASSWORD_BCRYPT);

    $sql = "UPDATE `admins` SET ";
    $sql .= "`first_name` = '" . mysqli_real_escape_string($db, $admin['first_name']) . "', ";
    $sql .= "`last_name` = '" . mysqli_real_escape_string($db, $admin['last_name']) . "', ";
    $sql .= "`email` = '" . mysqli_real_escape_string($db, $admin['email']) . "', ";
    if ($password_sent) {
        $sql .= "`hashed_password` = '" . mysqli_real_escape_string($db, $password) . "', ";
    }
    $sql .= "`username` = '" . mysqli_real_escape_string($db, $admin['username']) . "' ";
    $sql .= "WHERE `id` = '" . mysqli_real_escape_string($db, $admin['id']) . "' ";
    $sql .= "LIMIT 1";

    $result = mysqli_query($db, $sql);
    if ($result) {
        echo "here1";
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}

function removeAdmin($admin_id)
{
    global $db;
    $sql = "DELETE FROM `admins` ";
    $sql .= "WHERE `id` = '" . $admin_id . "'";
    $result = mysqli_query($db, $sql);
    if ($result) {
        return true;
    } else {
        echo mysqli_error($db);
        db_disconnect($db);
        die();
    }
}